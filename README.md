# Anno Designer Layouts

These are layouts for the game Anno 1800 created using the [Anno Designer](https://github.com/AnnoDesigner/anno-designer) tool. Feel free to use/edit them however you please.

Note: I did not create the layouts for the islands in `./base_islands/`.
